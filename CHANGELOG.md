# Change Log
All notable changes starting with the version 0.6.9 are documented here.

## [0.7.2]
- assert_eq! for floats replaced with assert_relative_eq! and assert_diff_abs_eq!
- CI pipeline for blas/lapack backends fixed
- Dev dependencies updated

## [0.7.1]
- Invalid URLs in README.md fixed

## [0.7.0]
- Using SemVer for release versioning
- log-normal distribution
- Different changes on beta, gamma and error functions

## [0.6
- Native Rust code, Openblas, Netlib, Intel-Mkl and Accelerate are now usable as linear algebra libraries

## [0.6.9]
- Eigen decomposition is implemented
- Implicit Euler
- Backward differentiation formula
