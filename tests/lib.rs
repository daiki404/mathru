#[macro_use]
extern crate mathru;

pub mod algebra;
pub mod analysis;
pub mod elementary;
pub mod num;
pub mod optimization;
pub mod special;
pub mod statistics;
